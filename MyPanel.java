package Interfata;

import java.awt.*;
import javax.swing.*;

class MyPanel extends JFrame {

	
	JPanel panel1, panel2, panel3, panel4;
	JLabel l1, l2, l3;
	JTextField tf1, tf2, tf3;
	JButton b1, b2, b3, b4, b5;
	
	public MyPanel(String nume)
	{
		setTitle(nume);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(480, 320);

		panel1 = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();
		panel4 = new JPanel();

		l1 = new JLabel("f1(x):");
		l2 = new JLabel("f2(x):");
		l3 = new JLabel("Rezultat:");
		
		tf1 = new JTextField("+0");
		tf2 = new JTextField("+0");
		tf3 = new JTextField("0");
		
		tf1.setColumns(30);
		tf2.setColumns(30);
		tf3.setColumns(30);

		panel1.add(l1);
		panel1.add(tf1);
		panel1.setLayout(new FlowLayout());
		panel2.add(l2);
		panel2.add(tf2);
		panel2.setLayout(new FlowLayout());
		panel4.add(l3);
		panel4.add(tf3);
		panel4.setLayout(new FlowLayout());

		b1 = new JButton("f1(x)+f2(x)");
		b2 = new JButton("f1(x)-f2(x)");
		b3 = new JButton("f1(x)*f2(x)");
		b4 = new JButton("Sf(x)");
		b5 = new JButton("f'(x)");
		
		panel3.add(b1);
		panel3.add(b2);
		panel3.add(b3);
		panel3.add(b4);
		panel3.add(b5);

		b1.addActionListener(new Butoane(0, tf1, tf2, tf3));
		b2.addActionListener(new Butoane(1, tf1, tf2, tf3));
		b3.addActionListener(new Butoane(2, tf1, tf2, tf3));
		b4.addActionListener(new Butoane(3, tf1, tf2, tf3));
		b5.addActionListener(new Butoane(4, tf1, tf2, tf3));
		
		JPanel p = new JPanel();
		p.add(panel1);
		p.add(panel2);
		p.add(panel3);
		p.add(panel4);
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

		setContentPane(p);
		setVisible(true);
	}
	
	public static void main(String args[]) {
		MyPanel frame = new MyPanel("Calculator polinoame");
	}

}