package Operatii;

public class Operatii {
	public static Polinom adunare(Polinom pA, Polinom pB) {
		Polinom rezultat = new Polinom();
		rezultat = pA.copy();
		boolean ok = true;
		for (Monom b : pB.getPolinom()) {
			for (Monom a : rezultat.getPolinom())
				if (b.putere == a.putere) {
					a.coeficient += b.coeficient;
					ok = false;

				}
			if (ok == true)
				rezultat.add(b);

		}
		return rezultat;
	}

	public static Polinom scadere(Polinom pA, Polinom pB) {
		Polinom rezultat = new Polinom();
		rezultat = pA.copy();
		for (Monom b : pB.getPolinom()) {
			boolean ok = true;
			for (Monom a : rezultat.getPolinom())
				if (b.putere == a.putere) {
					a.coeficient -= b.coeficient;
					ok = false;

				}
			if (ok == true)
				rezultat.add(new Monom(-b.coeficient, b.putere));

		}
		return rezultat;
	}

	public static Polinom inmultire(Polinom pA, Polinom pB) {
		Polinom rezultat = new Polinom();
		for (Monom b : pB.getPolinom())
			for (Monom a : pA.getPolinom()) {
				boolean ok = true;
				for (Monom p : rezultat.getPolinom())
					if (p.putere == a.putere + b.putere) {
						p.coeficient += a.coeficient * b.coeficient;
						ok = false;
					}
				if (ok == true)
					rezultat.add(new Monom(a.coeficient * b.coeficient, a.putere + b.putere));
			}
		return rezultat;
	}

	public static Polinom derivare(Polinom pA) {
		Polinom rezultat = new Polinom();
		rezultat = pA.copy();
		for (Monom a : rezultat.getPolinom())
			if (a.putere >= 1) {
				a.coeficient = a.coeficient * a.putere;
				a.putere--;
			} else
				a.coeficient = 0;

		return rezultat;
	}

	public static Polinom integrare(Polinom pA) {
		Polinom rezultat = new Polinom();
		rezultat = pA.copy();
		for (Monom a : rezultat.getPolinom()) {
			a.putere++;
			a.coeficient = (a.coeficient) / a.putere;

		}

		return rezultat;
	}

}