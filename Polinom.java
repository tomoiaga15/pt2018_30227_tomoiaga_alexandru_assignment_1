package Operatii;
import java.util.*;
import java.util.regex.*;

public class Polinom {

	List<Monom> p;
	String pattern = "([-+])(\\d+)?(x(\\^(\\d+))?)?"; // "(.*)(\\d+)(.*)";
	int coeficient, putere;
	boolean ok;

	public Polinom() {
		p = new ArrayList<Monom>();
	}

	public void consturctieP(String s) throws Exception {
		String line = s; // String to be scanned to find the pattern.
		Pattern r = Pattern.compile(pattern); // Create a Pattern object
		Matcher m = r.matcher(line); // Now create matcher object.
		int lungime = 0;
		while (m.find())
			if (!(m.group(2) == null && m.group(3) == null)) {
				if (m.group(2) == null)
					coeficient = 1;
				else
					coeficient = Integer.parseInt(m.group(2));
				String sign = m.group(1);
				if (sign.equals("-"))
					coeficient = -coeficient;
				if (m.group(3) == null)
					putere = 0;
				else if (m.group(4) == null)
					putere = 1;
				else
					putere = Integer.parseInt(m.group(5));
				check(new Monom(coeficient, putere));
				lungime += m.group(0).length();
			}
		checkSize(s.length(), lungime);
	}

	public void checkSize(int lString, int lPattern) throws Exception {
		if (lString != lPattern)
			throw new Exception("invalid");
	}

	public List<Monom> getPolinom() {
		return p;
	}

	public void check(Monom m) {
		boolean ok = true;
		for (Monom i : p)
			if (i.putere == m.putere) {
				i.coeficient += m.coeficient;
				ok = false;
				break;
			}
		if (ok == true) {
			p.add(new Monom(m.coeficient, m.putere));
		}
	}

	public String afisare() {
		String s = "";
		boolean ok = true, zero = true;
		for (Monom i : p) {
			if (i.getCoeficient() != 0)
					zero = false;
			s += i.toString(ok);
			if(zero==false)
			ok = false;
		}
		if (zero)
			s = "0";
		return s;

	}

	public void add(Monom m) {
		p.add(m);
	}

	public void clear() {
		p.clear();
	}

	public Polinom copy() {
		Polinom copie = new Polinom();
		for (Monom i : p) {
			copie.add(new Monom(i.getCoeficient(), i.getPutere()));
		}
		return copie;
	}

	public void sortare() {
		Collections.sort(p, new SortByPower());
	}

	class SortByPower implements Comparator<Monom> {
		public int compare(Monom a, Monom b) {
			return -(a.getPutere() - b.getPutere());
		}
	}
}
