package Operatii;

import java.text.*;

public class Monom {
	float coeficient;
	int putere;
	static DecimalFormat df = new DecimalFormat("#.##");

	public Monom(float coeficient, int putere) {
		this.coeficient = coeficient;
		this.putere = putere;
	}

	public float getCoeficient() {
		return this.coeficient;
	}

	public int getPutere() {
		return this.putere;
	}

	public String toString(boolean ok) {
		String s = "";
		if (coeficient == 0)
			return s;
		if (coeficient < 0) {
			if (coeficient == -1) {
				if (putere == 0)
					s += "-1";
				else
					s += "-";
			} else {
				s += df.format(coeficient);
			}
		} else {
			if (ok == false)
				s += "+";

			if (coeficient == 1) {
				if (putere == 0)
					s += "1";
			} else
				s += df.format(coeficient);

		}
		if (putere != 0)
			s += "x";
		if (putere > 1)
			s += "^" + putere;
		return s;
	}

}
