package Interfata;

import java.awt.event.*;
import javax.swing.*;
import Operatii.*;

public class Butoane extends Operatii implements ActionListener {

	int operatie;
	JTextField s1, s2;
	JTextField tf;

	public Butoane(int operatie, JTextField s1, JTextField s2, JTextField tf) {
		this.operatie = operatie;
		this.s1 = s1;
		this.s2 = s2;
		this.tf = tf;
	}

	public void actionPerformed(ActionEvent event) {

		Polinom a = new Polinom();
		Polinom b = new Polinom();
		Polinom r = new Polinom();
		boolean ok = true;
		try {
			a.consturctieP(s1.getText());
		} catch (Exception e) {
			tf.setText("Invalid polinom a");
			ok = false;
		}
		try {
			b.consturctieP(s2.getText());
		} catch (Exception e) {
			tf.setText("Invalid polinom b");
			ok = false;
		}
		if (ok == true) {
			a.sortare();
			System.out.println(a.afisare());
			b.sortare();
			System.out.println(b.afisare());
			switch (operatie) {
			case 0:
				r = adunare(a, b);
				break;
			case 1:
				r = scadere(a, b);
				break;
			case 2:
				r = inmultire(a, b);
				break;
			case 3:
				r = integrare(a);
				break;
			case 4:
				r = derivare(a);
				break;
			default:
				r = new Polinom();
			}
			System.out.println(r.afisare());
			r.sortare();
			tf.setText(r.afisare());
		}
	}

}
