package Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import Operatii.*;

class TestTest extends Operatii{
	public boolean validare(String s1, String s2, String c, int nrOp){
		Polinom a = new Polinom();
		Polinom b = new Polinom();
		Polinom r = new Polinom();
		try {
			a.consturctieP(s1);
		} catch (Exception e) {
			a.clear();
			return false;
		}

		try {
			b.consturctieP(s2);
		} catch (Exception e) {
			b.clear();
			return false;
		}
		a.sortare();
		b.sortare();
		switch (nrOp) {
		case 0:
			r = adunare(a, b);
			break;
		case 1:
			r = scadere(a, b);
			break;
		case 2:
			r = inmultire(a, b);
			break;
		case 3:
			r = integrare(a);
			break;
		case 4:
			r = derivare(a);
			break;
		default:
			r = new Polinom();
		}
		r.sortare();
		String s = r.afisare();
		a.clear();
		b.clear();
		if (s.equals(c))
			return true;
		else
			return false;
	}
	@Test
	void test() {
		assertTrue(validare("+x","+x","2x", 0));
		assertTrue(validare("+x","+x","0", 1));
		assertTrue(validare("+x","+x","x^2", 2));
		assertTrue(validare("+x","+x","0.5x^2", 3));
		assertTrue(validare("+x","+x","1", 4));
		
		assertTrue(validare("-x","-x","-2x", 0));
		assertTrue(validare("-x","-x","0", 1));
		assertTrue(validare("-x","-x","x^2", 2));
		assertTrue(validare("-x","-x","-0.5x^2", 3));
		assertTrue(validare("-x","-x","-1", 4));
		
		assertTrue(validare("+5x","-5x","0", 0));
		assertTrue(validare("+5x","-5x","10x", 1));
		assertTrue(validare("+5x","-5x","-25x^2", 2));
		assertTrue(validare("+5x","-5x","2.5x^2", 3));
		assertTrue(validare("+5x","-5x","5", 4));
		
		assertTrue(validare("+5x+3x^2+7","-5x+7x^3+1","7x^3+3x^2+8", 0));
		assertTrue(validare("+5x+3x^2+7","-5x+7x^3+1","-7x^3+3x^2+10x+6", 1));
		assertTrue(validare("+5x+3x^2+7","-5x+7x^3+1","21x^5+35x^4+34x^3-22x^2-30x+7", 2));
		assertTrue(validare("+5x+3x^2+7","-5x+7x^3+1","x^3+2.5x^2+7x", 3));
		assertTrue(validare("+5x+3x^2+7","-5x+7x^3+1","6x+5", 4));
		
		assertFalse(validare("+5x+3x^2+7 ","-5x+7x^3+1","7x^3+3x^2+8", 0));
		assertFalse(validare("+5x+3x^2+ 7","-5x+7x^3+1","-7x^3+3x^2+10x+6", 1));
		assertFalse(validare("+5x+3x^2 +7","-5x+7x^3+1","21x^5+35x^4+34x^3-22x^2-30x+7", 2));
		assertFalse(validare("+5x+3xm^2+7","-5x+7x^3+1","x^3+2.5x^2+7x", 3));
		assertFalse(validare("+5.x+3x^2+7","-5x+7x^3+1","6x+5", 4));
		
		assertTrue(validare("","","0", 0));
		assertTrue(validare("","","0", 1));
		assertTrue(validare("","","0", 2));
		assertTrue(validare("","","0", 3));
		assertTrue(validare("","","0", 4));
		
		assertFalse(validare("text care sa nu fie polinom","text care sa nu fie polinom", "0",3));
	}

}
